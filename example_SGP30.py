'''
example from :
https://stackoverflow.com/questions/46835247/python-smbus-lib-with-sensirion-sgp30
'''


import time
import sys
import smbus
bus = smbus.SMBus(1)
address = 0x58
reply_buffer = [0,0,0,0,0,0]
reply = [0,0]
# Write a block of 1 bytes '[0x03]' to address 'adress' from offset 0x20
bus.write_i2c_block_data(address, 0x20, [0x03])
time.sleep(.5)
while True:
    # Write a block of 1 bytes '[0x08]' to address 'adress' from offset 0x20
    bus.write_i2c_block_data(address,0x20,[0x08])
    time.sleep(.6)
    # Read a block of 6 bytes from address 'adress', offset 0
    block = bus.read_i2c_block_data(address,0,6)
    print('block : ',block)
    for i in range(0,6):
            reply_buffer[i]=block[i]
    for i in range(0,2):
            reply[i]=reply_buffer[i*3]
            reply[i]<<=8
            reply[i]|=reply_buffer[i*3+1]
    print('CO2eq = {} ppm TVOC = {} ppb'.format(reply[0],reply[1])) 
    time.sleep(1)