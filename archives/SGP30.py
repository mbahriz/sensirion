'''
Reading of an SGP30 gas sensor 
'''

import time
from sgp30 import SGP30
from smbus import SMBus
smbus = SMBus(1)



with SGP30(smbus) as chip:
    while True:
        aq = chip.measure_air_quality()
        V = aq.voc_ppb
        C = aq.co2_ppm
        print(V,C)
        time.sleep(1)