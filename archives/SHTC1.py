'''
Reading of an SHTC1 humidity and temperature sensor
'''

import smbus
import time

# adress of SHTC1 humidity and temperature sensor
addr = 0x70 # ecrit en hexdecimal avec le prefixe 0x
print('SHTC1 adress is : {} in decimal'.format(addr))
print('             is : {} in binary'.format(bin(addr)))
print('             is : {} in hexadecimal'.format(hex(addr)))
# registre / measurement comands of SHTC1
reg_ID = 0xEFC8
reg_T = [0x78,0x66] # 0x7866
reg_RH = 0x58E8
reg_0 = 0x00

# open bus 1 for the raspberry pi
bus = smbus.SMBus(1)
bus.write_i2c_block_data(addr,0x78,[0x66])
help(bus.write_i2c_block_data)
time.sleep(.5)
help(bus.read_i2c_block_data)
block = bus.read_i2c_block_data(addr,0)
#block = bus.read_i2c_block_data(addr,0x00,6)
print(block)