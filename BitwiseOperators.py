toto = 0xC8 # ecrit en hexdecimal avec le prefixe 0x
print(toto)
print('is : {} in decimal'.format(toto))
print('is : {} in binary'.format(bin(toto)))
print('is : {} in hexadecimal'.format(hex(toto)))

x = 42
# x << y
'''Returns x with the bits shifted to the left by y places
(and new bits on the right-hand-side are zeros).
This is the same as multiplying x by 2**y.
'''
print('x : ',x,bin(x))
y = x<<2
print('x<<2 : ',y,bin(y))
y<<=8
print('x>>2 : ',y,bin(y))